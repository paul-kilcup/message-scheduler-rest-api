# Message Scheduler Rest API

Goal:

Create a simple working example of a RESTful web service that can

- Schedule messages to be printed (To console)
- Accept message content and delivery time 
- Return HTTP status code of 202 Accepted
- Persist messages for recovery on restart

Assumptions

- Don't let perfect get in the way of good enough
- Get something working and ready to be refactored
- Prefer the reuse of existing tested components 
- Less code the better and well documented
- Performance and security is out of scope
- No Auth
- Messages must be between 1 and 2000 characters
- Delivery time must be a valid date-time value that occurs after the request is made


---

## I Chose to Use

- Node https://nodejs.org/en/download/
- Npm https://www.npmjs.com/package/npm
- Hapi https://www.npmjs.com/package/@hapi/hapi
- Joi https://www.npmjs.com/package/joi
- node-schedule https://www.npmjs.com/package/node-schedule
- moment https://www.npmjs.com/package/moment
- Lowdb https://www.npmjs.com/package/lowdb
- Postman https://www.postman.com/downloads/
- Newman https://www.npmjs.com/package/newman


---

## Install

1. Pre-req:  [Node 12.18.3+ and NPM 6.14.6+](https://nodejs.org/download/release/v12.18.3/) installed
2. Clone repo
3. Open a console window in the root folder of the repo
4. Run npm install


---

## Run

1. Open a console window in the root folder of the repo
2. Run node index.js
3. Or npm start

---

## Smoke Test

1. Open a console window in the root folder of the repo
2. Run the following:  npx newman run ./postman/MessageSchedulerRestAPI-SpotChecks.postman_collection.json -e ./postman/LocalDev.postman_environment.json -r cli,junit
3. Or npm test


---

## Try it Out

- curl 'http://localhost:3000/messages'
- [Postman folder](https://bitbucket.org/paul-kilcup/message-scheduler-rest-api/src/master/postman/) includes Postman collection and environment  



---

## API

- Swagger coming soon
- Example POST http://localhost:3000/messages

```
    {
        "msg": "In the year 3020 ... ",
        "dateToProcess": "3020-08-26T20:49:35.000Z"
    }
```


---

## Limits

- TBD
- Lowdb 200mb
- 


---

## License

- TBD


---

## Bugs

- null 
- If you don't test, that's the same as zero bugs - right? ;-)


---

## First Tests to Consider

Smoke

- Able to accept a valid POST request
- Able to reject an invalid POST request


Input

- Special characters in message
- Field Absent, Value missing, Empty value
- Long values
- Date:  Formats, Past
- Extra fields, duplicate fields
- ID collision
- No Json header
- Empty Json body
- Invalid Json
- ...

Message State

- After server restart
- Not processed and past delivery date
- Same request sent multiple times
- ...

Performance and Security

- Out of scope

Other

- ...


---

## Quality

To oversimplify...

- Quality = Perception - Expectations 

- Trying to **improve** quality by doing more tests,
is like trying to lose weight by weighing yourself more


---

## Issues

- No error handling 
- No explicit model
- No unit tests
- No end-to-end tests


---

## Todo 

- Resolve issues 
- Add unit tests
- Add end-to-end tests
- Use ESLint
- Refactor (Reorganize, create models, use a "real" db)
- Document API with swagger 


---

## Future Improvements

- Add ability to Auto-recover (https://www.npmjs.com/package/forever#usage)
- Automatically remove messages from persistence after message is printed
- Add better logging 
- Structure code for dev and prod execution 