/**
 * Node based RESTful web service that can schedule messages to be printed to the console
 * @summary Message Scheduler Rest API
 * @author Paul Kilcup <Paul.Kilcup@gmail.com>
 */

// Required node packages
const hapi = require('@hapi/hapi');
const joi = require('joi');
const moment = require('moment');
const schedule = require('node-schedule');
const _ = require('lodash');
const lowdb = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const loadashId = require('lodash-id');

// Datastore - Setup and seed data
const adapter = new FileSync('db.json');
const db = lowdb(adapter);
db._.mixin(loadashId);
db.defaults({ messages: [] })
  .write();
db.get('messages')
  .insert({ msg: 'Seed on startup', dateToProcess: moment().add(5, 'seconds').format() })
  .write();

// Schedule - Persisted messages
const savedMessage = db.get('messages').value();
const jobs = [];
_.forEach(savedMessage, (message) => {
  jobs.push(schedule.scheduleJob(Date.parse(message.dateToProcess), () => {
    console.log(`Scheduled Message:  DateToProcess:"${message.dateToProcess}" - Message:"${message.msg}" `);
  }));
});

// Define - server, routes, and handlers
const init = async () => {
  const server = hapi.server({
    port: 3000,
    host: 'localhost',
  });

  // Enable validation with Joi
  server.validator(joi);

  // GET - Return persisted messages
  server.route({
    method: 'GET',
    path: '/messages',
    handler: (request, h) => {
      const respBody = db.get('messages').value();
      return respBody;
    },
  });

  // POST - Persist message and schedule processing
  server.route({
    method: 'POST',
    path: '/messages',
    handler: (request, h) => {
      const jsonReq = request.payload;

      // Persist
      db.get('messages')
        .insert({ msg: jsonReq.msg, dateToProcess: jsonReq.dateToProcess })
        .write();

      // Schedule for processing
      const reqJob = schedule.scheduleJob(Date.parse(jsonReq.dateToProcess), () => {
        console.log(`Scheduled Message:  DateToProcess:"${jsonReq.dateToProcess}" - Message:"${jsonReq.msg}" `);
      });

      return h.response(jsonReq).code(202);
    },
    options: {
      auth: false,
      validate: {
        payload: {
          msg: joi.string().required().min(1).max(2000),
          dateToProcess: joi.date().required().greater('now'),
        },
      },
    },
  });

  // All other requests - Reject with 404
  server.route({
    method: '*',
    path: '/{any*}',
    handler(request, h) {
      return '404 Error! Page Not Found!';
    },
  });

  // Start server
  await server.start();
  console.log('Server running on %s', server.info.uri);
};

// Unhandled Error - Log and exit
process.on('unhandledRejection', (err) => {
  console.log(err);
  process.exit(1);
});

// Start server
init();
